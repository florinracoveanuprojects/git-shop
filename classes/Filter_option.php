<?php


class Filter_option extends BaseEntity
{
    public $filter_id;
    public $option_name;

    public function displayAll($id = null)
    {
        if (!is_null($id)) {
            $filterOptionRows = dbSelect(strtolower(static::class), ['filter_id' => $id]);
            foreach($filterOptionRows as $key => $filterOptionValue):?>
                <form action="" method="get">
                    <input type="checkbox" name="option" value="option" onchange="this.form.submit()"> <?php echo $filterOptionValue['option_name'];?><br>
                </form><?php
            endforeach;
        }
    }
}