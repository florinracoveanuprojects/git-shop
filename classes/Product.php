<?php

class Product extends BaseEntity
{

public $name;
public $full_name;
public $image;
public $description;
public $category_id;
public $price;
public $finalprice;
public $discount;

public function getFinalPrice() {
    $this->final_price = $this->price - $this->discount;
}

public function getCategories() {
    $cat = $this->category_id;
    $data = dbSelect('category',['parent_id'=>$cat]);
    $result = [];
    foreach ($data as $line) {
    $result[] = new Category($line['id']);
    }
    return $result;
}

public function getImages() {
    $id = $this->id;
    $data = dbSelect('product_images',['product_id'=>$id]);
    $result = [];
    foreach ($data as $line) {
        $result[] = new Image($line['id']);
    }
    return $result;
}

}