<?php


function dbInsert($table, $data) {
    global $conn;
    $col = [];
    $values = [];
    foreach ($data as $key => $val) {
        $col[] = mysqli_real_escape_string($conn, $key);
        $values[] = "'".mysqli_real_escape_string($conn, $val)."'";
    }
    $strCol = implode(",",$col);
    $strVal = implode(",",$values);
    $sql = "INSERT INTO $table ($strCol) VALUES ($strVal)";
    $conn->query($sql);
    return mysqli_insert_id($conn);

}

function dbUpdate($table, $id, $data){
    global $conn;
    $sets = [];
    foreach ($data as $column => $value){
        $sets[]="`".mysqli_real_escape_string($conn, $column)."`='".mysqli_real_escape_string($conn, $value)."'";
    }
    $sqlSets = implode(',', $sets);
    $sql = "UPDATE $table SET $sqlSets id=".intval($id);
    $conn->query($sql);
    return mysqli_affected_rows($conn)>0;
}

function dbDelete($table, $id){
    global $conn;
    $sql = "DELETE FROM $table WHERE id=".intval($id);
    $conn->query($sql);
    return mysqli_affected_rows($conn)>0;
}

function dbSelect($table, $filters=null, $likeFilters=null, $offset=0, $limit=null,  $sortBy=null, $sortDirection='ASC'){
    global $conn;
    $sql = "SELECT * FROM $table";
    if (($filters != null)||($likeFilters != null)){
        $sets = [];
        if ($filters != null) {
            foreach ($filters as $column => $value){
                if ($value != null) {
                    $sets[] = mysqli_real_escape_string($conn, $column)."='".mysqli_real_escape_string($conn, $value)."'";

                }
            }
        }
        if ($likeFilters != null) {
            foreach ($likeFilters as $column => $value){
                if ($value != null) {
                    $sets[] = mysqli_real_escape_string($conn, $column)." LIKE '%".mysqli_real_escape_string($conn, $value)."%'";
                }
            }
        }
        $sql.= ' WHERE '.implode(' AND ', $sets);
    }
    if ($sortBy != null) {
        $sql.= ' ORDER BY '.mysqli_real_escape_string($conn, $sortBy).' '.mysqli_real_escape_string($conn, $sortDirection);
    }
    if ($limit != null){
        $sql.= ' LIMIT '.intval($offset).','.intval($limit);
    }
    $result = mysqli_query($conn, $sql);
    if (!$result){
        die("SQL error: " . mysqli_error($conn)." SQL:".$sql);
    }
    return $result->fetch_all(MYSQLI_ASSOC);
}

function listare($produse, $ItemsPerRow) { ?>
    <br><br>
    <div class="row promo">
     <?php
     $i = 0; $par = 12/$ItemsPerRow;
     foreach ($produse as $line) {  ?>
        <div class="col-sm-<?php echo $par; ?> card-group text-center">
        <div class="card">
            <span class="test"><?php echo $line->discount . " RON"; ?></span>
            <a href="description.php?id=<?php echo $line->id; ?>"><img style="width:100%" class="card-img-top" src="images/<?php echo $line->image; ?>"></a>
            <div class="card-body">
                <a id="list" class="card-title" href="description.php?id=<?php echo $line->id; ?>"><?php echo $line->name; ?></a>
            </div>
            <div class="card-footer deleteRowSpaces">
                <?php $line->getFinalPrice(); ?>
                <p class="card-text"><del><?php echo $line->price . " RON"; ?></del></p>
                <p class="card-pret"><b><?php echo $line->final_price . " RON"; ?></b></p>
                <a class="cos btn" href="add_cos.php?produs=<?php echo $line->id; ?>"><i class='fas fa-shopping-cart'></i> &nbsp Adauga &nbsp </a>
             </div>
        </div>
    </div>
        <?php $i++;
              if ($i % $ItemsPerRow  == 0) { ?>
        </div>
        <div class="row promo">
        <?php }
      }
      echo "</div>";
}

function display($produs, $id){
    $image = new Image();
    $filter['product_id'] = $id;
    $selImage = $image->imageFilter($filter);
    // $image = dbSelect('product_images', ['product_id' => $id], null,null,0, 1);?>
    <div class="col-sm-2 card-group text-center">
        <div class="card">
            <span class="test"><?php echo $produs->discount . " RON"; ?></span>
            <a href="description.php?id=<?php echo $id; ?>"><img style="width:100%" class="card-img-top" src="images/<?php echo $selImage[0]->url; ?>"></a>
            <div class="card-body">
                <a id="list" class="card-title" href="description.php?id=<?php echo $produs->id; ?>"><?php echo $produs->name; ?></a>
            </div>
            <div class="card-footer deleteRowSpaces">
                <?php $pret = $produs->price - $produs->discount; ?>
                <p class="card-text"><b><del><?php echo $produs->discount + $pret . " RON"; ?></del></b></p>
                <p><?php echo $pret . " RON"; ?></p>
            </div>
        </div>
    </div><?php
}

function dispItemCos($item, $nr) {  ?>

    <div class="row cos-list">
        <div class="col-sm-7">
            <b>PRODUS</b>
        </div>
        <div class="col-sm-1">
            <b>Cantitate</b>
        </div>
        <div class="col-sm-2 cos-col">
            <b>Pret unitar</b>
        </div>
        <div class="col-sm-2 cos-col">
            <b>Subtotal</b>
        </div>
    </div><br>
    <div class="row">
            <div class="col-sm-1 cos-item">
            <a href="description.php?id=<?php echo $item->id; ?>"><img class="cos-img" src="images/<?php echo $item->image; ?>"></a>
            </div>
            <div class="col-sm-6 cos-desc">
            <a id="cos-desc" href="description.php?id=<?php echo $item->id; ?>"><?php echo $item->full_name; ?></a>
            </div>
            <div class="col-sm-1 cos-col">
                <b><?php echo $nr; ?></b><br><br>
                <a class="cos-op" href="mp_cos.php?op=1&itemID=<?php echo $item->id; ?> "><input type="button" value="+"></a>&nbsp
                <a class="cos-op" href="mp_cos.php?op=0&itemID=<?php echo $item->id; ?>"><input type="button" value="-"></a>
            </div>
            <div class="col-sm-2 text-danger cos-col">
                <b><?php echo $item->finalprice; ?></b>
            </div>
            <div class="col-sm-2 text-danger cos-col">
                <b><?php echo $item->finalprice*$nr; ?></b>
            </div>
    </div><br>
    <?php   $valItemCos = $item->finalprice*$nr;
            return $valItemCos;
 }

function dispCos($cos) {   //functia numara cate produse identice avem in cos
    $prodCos = [];
    foreach ($cos as $val) {
       $key = strval($val);
       $prodCos[$key]=0;
    }
    foreach ($cos as $val) {
       $key = strval($val);
       $prodCos[$key]++;
    }
    return $prodCos;
    }

function dbSelectOne($table, $filters=[], $likeFilters=[], $offset=0, $limit=null,  $sortBy=null, $sortDirection='ASC'){
    $data = dbSelect($table, $filters, $likeFilters, 0, 1,  $sortBy, $sortDirection);
    return $data[0];
}

function getLastId($table) {
    global $conn;
    $sql = "SELECT id FROM $table WHERE LIMIT 1 ORDER BY id DESC";
    $res = $conn->query($sql);
    if (!$res) {
        return null;
        die();
    }
    $result = $res->fetch_assoc();
    return $result['id'];
    }


//am creat si functia pentru afisarea review-urilor, dar nu o mai folosesc acum, folosind clase.
function selectReviews($id){
    $reviews = dbSelect('review', ['product_id' => $id]);
    foreach($reviews as $review):?>
        <div class="row" >
            <div class="col-sm-3">
                <p><b><?php echo $review['username']; ?></b></p><br />
                <p><?php echo $review['add_date']; ?></p>
            </div>
            <div class="col-sm-9">
                <h4><?php echo $review['title']; ?></h4><?php
                for($i = 1; $i <= 5; $i++){
                    if($i <= $review['rating']){
                        ?><span class="fa fa-star checked"></span><?php
                    }else{
                        ?><span class="fa fa-star "></span><?php
                    }
                }
                ?>
                <p><?php echo $review['message']; ?></p>
            </div>
        </div><?php
    endforeach;

}