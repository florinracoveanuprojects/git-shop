<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PC NET  produse IT</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container-fluid" style="width:85%">

    <?php
    session_start();
    include "includes/config.php";
    include "includes/functions.php";
    include "parts/header.php";
    include "parts/meniu.php";

    if (isset($_GET['page'])) {
        $pag = $_GET['page'];
    }
    else {$pag = 1;}
    $ItemsPerPage = 12;
    $start = ($pag-1)*$ItemsPerPage;
    $table = 'product';
    $sortBy = 'finalprice';
    $dir = 'ASC';
    $selCat['category_id'] = $_GET['cat'];

    $prod = new Product();
    $productFil = $prod->select($selCat,null, $start, $ItemsPerPage, null, $dir);

    $categories = dbSelectOne('category',['id' => $_GET['cat']]);
    ?>
    <div class="row">
        <h4>Esti aici: <a href="categories.php?cat=<?php echo $_GET['cat'];?>"><?php echo $categories['name']; ?></a></h4><br />
    </div><hr>
    <div class="row">
        <div class="col-sm-2 jumbotron">
            <?php //include "category_sidebar.php";?>
        </div>
        <div class="col-sm-10">
    <?php
    listare($productFil, 6);
    //$products = dbSelect($table, $selCat, 0, $start, $ItemsPerPage, $sortBy );
    $total = dbSelect($table, $selCat);
    $ItemsNr = count($total); // numar total de elemente care corespund criteriului de filtrare
    $totalPages = ceil($ItemsNr/$ItemsPerPage);
    ?>
        </div>
    </div><br /><?php
    include "parts/pagination.php";

    include "parts/footer.php";
    ?>
</div>
</body>
</html>