<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PC NET  produse IT</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
          integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container-fluid" style="width:85%">
<?php
include "includes/config.php";
include "includes/functions.php";
include "parts/header.php";
include "parts/meniu.php";
?>
<br><br><br>
<div class="row">
    <div class="col">
        <h3>Creaza un cont</h3>
        <form action="add_cont.php" method="post">
            <div class="form-group">
                <input type="text" class="form-control" name="prenume" placeholder="Prenume">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="nume" placeholder="Nume">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="email" placeholder="E-mail">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="tel" placeholder="Numar de telefon">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="parola" placeholder="Parola">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="confpar" placeholder="Confirma parola">
            </div>
            <button type="submit" class="btn btn-primary">Inregistrare</button><br>
            <h4><?php if(isset( $_GET['signInError'])) echo $_GET['signInError']; ?></h4>
        </form>
        <br><br>
    </div>
    <div class="col">
        <h3>Intra in cont</h3>
        <form action="auth.php" method="post">
            <div class="form-group">
                <input type="text" class="form-control" name="email" placeholder="E-mail">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="parola" placeholder="Parola">
            </div>
            <button type="submit" class="btn btn-primary">Login</button><br>
            <h4><?php if(isset( $_GET['logInError'])) echo $_GET['logInError']; ?></h4>
        </form>
        <br><br>
    </div>
</div>
</div>

</body>
</html>