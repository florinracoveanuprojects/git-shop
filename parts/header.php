
<div class="row hd1">
    <div class="col-sm-3 my-auto">
        <a href="index.php"><img src="images/logo.jpg" width="120"></a>
    </div>
    <div class="col-sm-6 my-auto">
        <form class="form-inline" action="cautare.php" method="post">
            <div class="input-group md-form form-sm form-2 pl-0">
                <input class="form-control my-0 py-1 red-border" type="text" placeholder="Cauta produs" name="produs" aria-label="Search">
                <div class="input-group-append">
                    <button type="submit" class="btn btn-primary">Cauta</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-sm-3 my-auto">
        <div class="right"><?php
            if(isset($_SESSION['user'])){?>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><?php echo ucfirst($_SESSION['user']); ?></button>
                    <ul class="dropdown-menu">
                        <?php echo "Salut, ".ucfirst($_SESSION['user'])." !"; ?>
                        <li><a href="">Contul meu</a></li>
                        <li><a href="">Setari</a></li>
                        <li><a href="">Comenzile mele</a></li>
                        <?php
                        $user = dbSelectOne('users',['firstname' => $_SESSION['user']]);
                        if($user['admin'] == 1){
                            ?><li><a href="panou_administrare.php">Panou de administrare</a></li><?php
                        }
                        ?>
                        <li><a href="?action=logout">Deconectare</a></li>
                    </ul>
                    <a class="hed" href="list_cos.php"><i class='fas fa-shopping-cart'></i> Cosul meu </a>
                </div>
                <?php
                if(isset($_GET['action'])){
                    session_destroy();
                    header('Location: index.php');
                }
            }else{ ?>
                <a class="hed" href="cont.php?eroare= &auth= ">Cont &nbsp&nbsp&nbsp </a>
                <a class="hed" href="list_cos.php"><i class='fas fa-shopping-cart'></i> Cosul meu </a><?php
            } ?>
        </div>
    </div>
</div>
