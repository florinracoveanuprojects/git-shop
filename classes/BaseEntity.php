<?php


class BaseEntity
{
    public $id;

    public function __construct($id = null)
    {
        if (!is_null($id)) {
            $data = dbSelectOne($this->getTable(), ['id' => $id]);
            foreach ($data as $key => $val) {
                $this->$key = $val;
            }
        }
    }

    public function getTable()
    {
        return strtolower(static::class);
    }

    public function delete()
    {
        dbDelete($this->getTable(), $this->id);
    }

    public function save()
    {
        if (!is_null($this->id)) {
            dbUpdate($this->getTable(), $this->id, get_object_vars($this));
        } else {
            $this->id = dbInsert($this->getTable(), get_object_vars($this));
        }
    }

    public function select($filters, $likeFilters, $offset, $limit,  $sortBy, $dir)
    {
        $data = dbSelect($this->getTable(), $filters, $likeFilters, $offset, $limit, $sortBy, $dir);
        $result = [];
        foreach ($data as $line) {
            if ($this->getTable() == 'product') {
                $result[] = new Product($line['id']);
            }
            if ($this->getTable() == 'product_images') {
                $result[] = new Image($line['id']);
            }
            if ($this->getTable() == 'orders') {
                $result[] = new Orders($line['id']);
            }
            if ($this->getTable() == 'review') {
                $result[] = new Review($line['id']);
            }
            if ($this->getTable() == 'category') {
                $result[] = new Category($line['id']);
            }
            if ($this->getTable() == 'order_items') {
                $result[] = new Order_items($line['id']);
            }
        }

        return $result;
    }

    public function selectOne($filter)
    {
        $data = dbSelectOne($this->getTable(), $filter);
        foreach ($data as $key => $val) {
            $this->$key = $val;
        }
    }

    public function lastID()
    {
       $this->id = getLastId($this->getTable());
    }
}

