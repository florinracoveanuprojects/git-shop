<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PC NET  produse IT</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container-fluid" style="width:85%">

    <?php
    session_start();
    include "includes/config.php";
    include "includes/functions.php";
    include "parts/header.php";
    include "parts/meniu.php";
    if(isset($_GET['id'])){
        $_SESSION['id'] = $_GET['id'];
    }
    $desFilter['id'] = $_SESSION['id'];
    $produs = new Product();
    $produs->selectOne($desFilter);
    $img = new Image();
    $imgFilter['product_id'] = $produs->id;
    $selImages = $img->select($imgFilter, null, 0, null, null, null);
    // $images = dbSelect('product_images',['product_id' => $produs['id']]);?>
    <br />
    <div class="row text-center">
        <div class="col-sm-5">
            <span class="dicount">-<?php echo $produs->discount;?></span>
            <img src="images/<?php echo $produs->image; ?>" width="300"><br/><br/>
            <?php foreach($selImages as $lineImg):?>
                <img src="images/<?php echo $lineImg->url; ?>" width="100"><?php
            endforeach; ?>
        </div>
        <div class="col-sm-7">
            <div class="row">
                <div class="col-sm-12">
                    <h5><?php echo $produs->full_name; ?></h5><hr>
                </div>
                <div class="col-sm-6">
                    <h4 style="color:red"><b><?php echo $produs->price - $produs->discount; ?> RON cu TVA</b></h4>
                    <p><b>Pret vechi <del><?php echo $produs->price; ?> RON</del></b></p>
                    <p>Vandut si livrat de: </p>
                    <?php
                    $review = new Review();
                    ?>

                    <p>Opinia clientilor: <?php echo $review->getRating($_GET['id']); ?></p>

                    <?php for($i = 1; $i <= 5; $i++){
                        if($i <= $review->getRating($_GET['id'])){
                            ?><span class="fa fa-star checked"></span><?php
                        }else{
                            ?><span class="fa fa-star"></span><?php
                        }
                    } ?>
                    <br />
                    <a href="#review"><?php echo $review->getTotalReviews($_GET['id']); ?> review-uri</a><a href="">| x intrebari</a><br />
                    <a href="review.php?id=<?php echo $_GET['id'];?>">Adauga review</a>
                </div>
                <div class="col-sm-6">
                    <a href="add_cos.php?produs=<?php echo $produs->id; ?>"> <button type="button" class="btn btn-primary btn-lg btn-block">Adauga in cos</button></a>
                    <br/>
                    <button type="button" class="btn btn-danger btn-lg btn-block">Adauga la favorite</button>
                    <br/>
                    <a href=""><p><i class="fa fa-shopping-cart"></i>Cum cumpar/Livrare</p></a>
                    <p><b>Garantie inclusa: </b><a href="">detalii</a></p>
                </div>
            </div>
        </div>
    </div><hr>
    <br/>
    <div class="row">
        <div class="col-sm-12">
            <h4>Descriere</h4>
            <p><?php echo $produs->description; ?></p>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-sm-12">
            <h4>Caracteristici</h4>
            <p><?php ?></p>
        </div>
    </div><hr>
    <div class="container-fluid" style="background-color: #d9d9d9">
        <h4>Te-ar putea interesa si</h4><br>
        <div class="row" >
            <?php 
            $prodSim = new Product();
            // $produseSimilare = dbSelect('products', ['category_id' => $produs['category_id']],0,6);
            $simFilter['category_id'] = $produs->category_id;
            $produseSimilare = $prodSim->select($simFilter, null, 0, 6, null, null);
            foreach($produseSimilare as $produsSimilar){
                if($produsSimilar->id != $_SESSION['id']) {
                    display($produsSimilar, $produsSimilar->id);
                }
            } ?>
        </div><br />
    </div><br />
    <div class="row">
        <div class="col-sm-12" style="background-color: skyblue">
            <h4>Istoricul tau</h4>

        </div>
    </div>
    <hr />
    <h4 id="review">Review-uri</h4>
    <?php //selectReviews($_GET['id']);
    $reviews = $review->select(['product_id' => $_GET['id']], null, 0, null, null, null);
    for($i = 0; $i <count($reviews); $i++){
        $review->getReviewsByID($reviews[$i]->id);
        ?>
        <div class="row" >
            <div class="col-sm-3">
                <p><b><?php echo $review->username; ?></b></p><br />
                <p><?php echo $review->add_date; ?></p>
            </div>
            <div class="col-sm-9">
                <h4><?php echo $review->title; ?></h4><?php
                for($j = 1; $j <= 5; $j++){
                    if($j <= $review->rating){
                        ?><span class="fa fa-star checked"></span><?php
                    }else{
                        ?><span class="fa fa-star "></span><?php
                    }
                } ?>
                <p><?php echo $review->message; ?></p>
            </div>
        </div>
        <hr><?php
    }
    ?>
    <hr />
    <?php include "parts/footer.php";
    ?>
</div>
</body>
</html>


