<?php
include "includes/config.php";
include "includes/functions.php";

session_start();

$com = new Orders();

$com->firstname = $_POST['prenume'];
$com->lastname = $_POST['nume'];
if (($com->firstname == null)||($com->lastname == null)){
    header("Location: fin_comand.php?formError=Trebuie completate campurile obligatorii");
    die();
}
$com->firstname = filter_var($com->firstname, FILTER_SANITIZE_STRING);
$com->lastname = filter_var($com->lastname, FILTER_SANITIZE_STRING);

$com->email = $_POST['email'];
if ($com->email == null) {
    header("Location: fin_comand.php?formError=Trebuie completat campul de adresa de email");
    die();
}
$com->email = filter_var($com->email, FILTER_SANITIZE_EMAIL);
if (filter_var($com->email, FILTER_VALIDATE_EMAIL) === false) {
    header("Location: fin_comand.php?formError=Nu este o adresa de email valida");
    die();
}
$com->status = $_POST['statut'];
$com->phone = $_POST['tel'];
$com->street = $_POST['strada'];
if (($com->phone == null)||($com->street == null)){
    header("Location: fin_comand.php?formError=Trebuie completate campurile obligatorii");
    die();
}
$com->phone = filter_var($com->phone,FILTER_SANITIZE_STRING);
$com->street = filter_var($com->street, FILTER_SANITIZE_STRING);

$com->county = $_POST['judet'];
$com->city = $_POST['oras'];
if (($com->county == null)||($com->city == null)){
    header("Location: fin_comand.php?formError=Trebuie completate campurile obligatorii");
    die();
}
$com->county = filter_var($com->county, FILTER_SANITIZE_STRING);
$com->city = filter_var($com->city, FILTER_SANITIZE_STRING);

$com->pay = $_POST['plata'];
$com->value = $_SESSION['valCos'];
$com->year = date("Y");
$com->month = date("m");
$com->day = date("d");

$com->save();

$orderItem = new Order_items();
$prodItem = new Product();

$stmt = $conn->prepare("INSERT INTO order_items (prod_id, price, quantity, order_id, category_id) VALUES (?, ?, ?, ?, ?)");
$stmt->bind_param("idiii", $orderItem->prod_id, $orderItem->price, $orderItem->quantity, $orderItem->order_id, $orderItem->category_id);

foreach($_SESSION['totalCos'] as $key => $sum) {
   $orderItem->prod_id = intval($key);
   $orderItem->quantity = $sum;
   $prodItem->selectOne(['id' => intval($key)]);
   $orderItem->price = $prodItem->finalprice;
   $orderItem->category_id = $prodItem->category_id;
   $orderItem->order_id = $com->id;
   $stmt->execute();
   // $orderItem->save();
   }
$to = "$com->email";
$headers = "From: sales@pcnet.ro";
$msg = "Comanda dvs. a fost inregistrata cu succes.";

mail($to,"Comanda dvs. la PC NET", $msg);

header("Location: list_cos.php");

