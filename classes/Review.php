<?php


class Review extends BaseEntity
{

public $username;
public $add_date;
public $product_id;
public $rating;
public $message;
public $title;

    public function getTotalReviews($id){
        $reviews = dbSelect('review', ['product_id' => $id]);
        return count($reviews);
    }
    public function getRating($id){
        $totalRating = 0;
        $reviews = dbSelect('review', ['product_id' => $id]);
        foreach($reviews as $value):
            $totalRating += $value['rating'];
        endforeach;
        if(count($reviews) != 0) {
            $finalRating = $totalRating / count($reviews);
        }else{
            $finalRating = 0;
        }
        return $finalRating;
    }
    public function getReviewsByID($id){
        $reviewsByID = dbSelectOne('review', ['id' => $id]);
        if($reviewsByID != null) {
            foreach ($reviewsByID as $key => $value) {
                $this->$key = $value;
            }
        }
    }

}