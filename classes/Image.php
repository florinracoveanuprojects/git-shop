<?php

class Image extends BaseEntity
{

public $url;
public $product_id;

public function getTable()
  {
   return "product_images";
  }

public function imageFilter($filter) {
    $data = dbSelect('product_images', $filter, null, 0, null, null);
    $result = [];
    foreach ($data as $line) {
        $result[] = new Image($line['id']);
    }
    return $result;
}
}

