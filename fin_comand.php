<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PC NET  produse IT</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php session_start();

if (isset($_GET['formError'])) {
    $formError = $_GET['formError'];
}
else {$formError = " ";} ?>

<div class="container-fluid" style="width:85%">
    <a id="cos2" href="index.php"><h4> Acasa </h4></a>/ Detalii comanda
    <hr><br><br>
    <a href="list_cos.php" class="acos"> << Inapoi la cos &nbsp</a>
    <hr><br><br>
    <div class="row">
        <div class="col-sm-9">
            <h3>Adresa de facturare</h3><hr><br>
            <h5>DATE DE CONTACT</h5><br>
            <form action="send_com.php" method="post">
                <div class="form-group w-50">
                   <label for="pren">Prenume:</label><br>
                    <?php if (!isset($_SESSION['user'])) { ?>
                   <input type="text" class="form-control" id="pren" name="prenume"> <?php }
                   else { ?>
                   <input type="text" class="form-control" id="pren" value="<?php echo $_SESSION['user']; ?>" name="prenume"> <?php } ?>
                </div>
                <div class="form-group w-50">
                    <label for="num">Nume:</label><br>
                    <?php if (!isset($_SESSION['userName'])) { ?>
                    <input type="text" class="form-control" id="num" name="nume"> <?php }
                    else { ?>
                    <input type="text" class="form-control" id="num" value="<?php echo $_SESSION['userName']; ?>" name="nume"> <?php } ?>
                </div>
                <div class="form-group">
                    <label for="em">Email:</label>
                    <?php if (!isset($_SESSION['email'])) { ?>
                    <input type="email" class="form-control" id="em" name="email"> <?php }
                    else { ?>
                    <input type="email" class="form-control" id="em" value="<?php echo $_SESSION['email']; ?>" name="email"> <?php } ?>
                </div>
                <br><br>
            <h5>ADRESA</h5><br>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="statut" value="persoana" checked>Persoana
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="statut" value="companie">Companie
                    </label>
                </div><br><br><br>
                <div class="form-group w-50">
                    <label for="tel">Numar de telefon:</label><br>
                    <?php if (!isset($_SESSION['phone'])) { ?>
                    <input type="text" class="form-control" id="tel" name="tel"> <?php }
                    else { ?>
                    <input type="text" class="form-control" id="tel" value="<?php echo $_SESSION['phone']; ?>" name="tel"> <?php } ?>
                </div>
                <div class="form-group">
                    <label for="str">Strada:</label>
                    <textarea class="form-control" rows="3" name="strada" id="str"></textarea>
                </div>
                <div class="form-group w-50">
                    <label for="jud">Judet</label><br>
                    <input type="text" class="form-control" id="jud" name="judet">
                </div>
                <div class="form-group w-50">
                    <label for="ora">Oras</label><br>
                    <input type="text" class="form-control" id="ora" name="oras">
                </div>
            <br><br>
            <h3>Detalii plata</h3><hr><br>
                <div class="form-check">
                     <label class="form-check-label">
                         <input type="radio" class="form-check-input" name="plata" value="numerar" checked>Numerar la livrare
                     </label>
                </div>
                <hr>
                <div class="form-check">
                     <label class="form-check-label">
                         <input type="radio" class="form-check-input" name="plata" value="card">Online cu card bancar
                     </label>
                </div>
                <hr>
                <div class="form-check">
                     <label class="form-check-label">
                         <input type="radio" class="form-check-input" name="plata" value="ordin">Ordin de plata
                     </label>
                </div>
                <hr>
                <br><br>
                <div class="form-check">
                      <button type="submit" class="btn btn-danger w-100 fin-but">Finalizeaza comanda</button>
                </div>
            </form>
            <br><br><br>
            <h4 style="color:red;"><?php echo $formError; ?></h4><br>
        </div>
        <div class="col-sm-3">
            <h3>Sumar</h3><hr>
            <h6>TOTAL:</h6>
            <h3 style="color:darkblue;"><b><?php echo $_SESSION['valCos']." LEI"; ?></b></h3>
        </div>
    </div>
    <?php include "parts/footer.php"; ?>
</div>
</body>
</html>





