<?php
class Category extends BaseEntity
{

public $name;
public $parent_id;

public function __construct($id=null) {
    if ($id !== null) {
        $data = dbSelectOne('category', ['id' => $id]);
        foreach ($data as $key => $val) {
            $this->$key = $val;
        }
    }
}

public function getProdCat($cat) {
    $data = dbSelect('product',['category_id'=>$this->id]);
    $result = [];
    foreach ($data as $line) {
        $result[] = new Product($line['id']);
    }
    return $result;
    }

public function catFilter($filters) {
    $data = dbSelect('category', $filters);
    $result = [];
    foreach ($data as $line) {
        $result[] = new Category($line['id']);
    }
    return $result;
}
}