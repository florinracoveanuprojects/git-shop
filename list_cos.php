<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PC NET  produse IT</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container-fluid" style="width:85%">

<?php include "includes/config.php";
      include "includes/functions.php";
      session_start(); ?>


        <a id="cos1" href="index.php"><h4> Acasa </h4></a>
        <hr><br><br>
        <h3>Cos de cumparaturi</h3><hr><br>

    <?php
      if ((!isset($_SESSION['cos']))||($_SESSION['cos'] == [])) {
          echo "<h3> Cosul este gol </h3>";
      }
      else {
          $_SESSION['totalCos'] = dispCos($_SESSION['cos']);
          $valCos = 0;
          $itemCos = new Product();
      foreach ($_SESSION['totalCos'] as $keyID => $suma) {
            $selItem['id'] = intval($keyID);
            $itemCos->selectOne($selItem);
            // $itemCos = dbSelect('products', $selItem);
            $valItem = dispItemCos($itemCos, $suma);
            $valCos = $valCos + $valItem;
      }
      $_SESSION['valCos'] = $valCos; ?> <br><hr>
<div class="row">
    <div class="col-sm-10">
        <a href="empty.php">
            <button type="submit" class="btn btn-primary">Goleste cosul</button></a>
     </div>
    <div class="col-sm-2 cos-col">
        <h4> Total cos </h4><br>
        <h4 style="color:red"><?php echo $valCos." RON"; ?></h4>
    </div>
</div>
<br><hr><br><br>
<div class="row">
    <a class="fin-com" href="fin_comand.php">Finalizeaza comanda</a>
</div>
<br><br><br>
<?php }
    include "parts/footer.php"; ?>
</div>

</body>
</html>


