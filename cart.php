<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PC NET  produse IT</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
          integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container-fluid" style="width:85%">

    <?php
    session_start();
    include "includes/config.php";
    include "includes/functions.php";
    include "parts/header.php";
    include "parts/meniu.php";
    global $conn;?>
    <div class="row cos-list">
        <div class="col-sm-7">
            <b>PRODUS</b>
        </div>
        <div class="col-sm-1">
            <b>Cantitate</b>
        </div>
        <div class="col-sm-2 cos-col">
            <b>Pret unitar</b>
        </div>
        <div class="col-sm-2 cos-col">
            <b>Subtotal</b>
        </div>
    </div><br><?php
    if(isset($_SESSION['userId'])) {
        $result = mysqli_query($conn, "SELECT * FROM order_items INNER JOIN products ON products.id = order_items.prod_id WHERE customer_id = '" . $_SESSION['userId'] . "'");
        $orderItems = $result->fetch_all(MYSQLI_ASSOC);
        $valCos = intval(displayCartProducts($orderItems, 0));
        ?>
        <div class="row">
        <div class="col-sm-10">
            <a href="empty.php">
                <button type="submit" class="btn btn-primary">Goleste cosul</button>
            </a>
        </div>
        <div class="col-sm-2 cos-col">
            <h4> Total cos </h4><br>
            <h4 style="color:red"><?php echo $valCos . " RON"; ?></h4>
        </div>
        </div><?php
    }else{
        $valCos = intval(displayCartProducts($_SESSION['product'], 0));
        ?>
        <div class="row">
        <div class="col-sm-10">
            <a href="empty.php">
                <button type="submit" class="btn btn-primary">Goleste cosul</button>
            </a>
        </div>
        <div class="col-sm-2 cos-col">
            <h4> Total cos </h4><br>
            <h4 style="color:red"><?php echo $valCos . " RON"; ?></h4>
        </div>
        </div><?php
    }
    include "parts/footer.php";
    ?>
</div>
</body>
</html>