<?php
include "includes/config.php";
include "includes/functions.php";

$newUser = new Users;
$newUser->firstname = $_POST['prenume'];
$newUser->lastname = $_POST['nume'];
$newUser->email = $_POST['email'];
$newUser->phone = $_POST['tel'];
$newUser->password = $_POST['parola'];

$conf = $_POST['confpar'];

if (($newUser->firstname == null)||($newUser->lastname == null)||($newUser->email == null)) {
    header('Location: cont.php?signInError=Trebuie completate campurile obligatorii!');
    die();
}
if ($newUser->password == null) {
    header('Location: cont.php?signInError=Trebuie completata parola!');
    die();
}

$newUser->firstname = filter_var($newUser->firstname, FILTER_SANITIZE_STRING);
$newUser->lastname = filter_var($newUser->lastname, FILTER_SANITIZE_STRING);
$newUser->phone = filter_var($newUser->phone, FILTER_SANITIZE_STRING);
$newUser->password = filter_var($newUser->password, FILTER_SANITIZE_STRING);
if (filter_var($newUser->email, FILTER_VALIDATE_EMAIL) === false) {
    header("Location: cont.php?signInError=Nu este o adresa de email valida");
    die();
}
$newUser->email = filter_var($newUser->email, FILTER_SANITIZE_STRING);

if ($conf == $newUser->password){
    $selUser = new Users;
    $newUser->password = password_hash($newUser->password, PASSWORD_DEFAULT);
    $selUser->selectOne(['email' => $newUser->email]);
    if ($selUser->email == null) {
        $newUser->save();
        header("Location: index.php");
    } else {
        header('Location: cont.php?signInError=Emailul exista!');
    }
}
else {
    header('Location: cont.php?signInError=Parola confirmata nu coincide cu cea introdusa initial!');
}

